package polymorphism.inclass;

public class Dog extends Animal {
	
	protected void speak() {
		super.speak();
		System.out.println("Woof Woof!");
	}
	
	protected void eat() {
		System.out.println("Like all dogs, I love to eat " + this.getFood());
	}
	
	protected void move() {
		System.out.println("I walk or I run!");
	}
	
	
}
