package polymorphism.game;

public class Player extends Unit {

	public Player(String name, int attackPower, int healthPoints) {
		super(name, attackPower, healthPoints);
	}

	@Override
	public void specialPower(Unit unit) {
		System.out.println(this.name + " heals " + unit.name + " for 20hp!");
		unit.healthPoints += 20;
	}
}
