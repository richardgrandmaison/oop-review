package polymorphism.game;

public class BossClass extends Unit {

	public BossClass(String name, int attackPower, int healthPoints) {
		super(name, attackPower, healthPoints);
	}

	@Override
	//The boss has a powerful attack that does double normal attack damage!
	public void specialPower(Unit unit) {
		System.out.println(this.name + " hits " + unit + " for 40 damage!");
		unit.healthPoints -= this.attackPower * 2;
	}

}
