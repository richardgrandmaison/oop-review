package polymorphism.game;

public class NormalEnemy extends Unit {

	public NormalEnemy(String name, int attackPower, int healthPoints) {
		super(name, attackPower, healthPoints);
	}

	@Override
	//This enemy transfers 5 hp from its target to itself
	public void specialPower(Unit unit) {
		System.out.println(this.name + " steals 5 hp from " + unit.name);
		unit.healthPoints -= 5;
		this.healthPoints += 5;
	}

}
